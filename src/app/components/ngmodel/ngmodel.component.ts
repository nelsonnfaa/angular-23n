import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.scss']
})
export class NgmodelComponent implements OnInit {
  secondInputText: string;
  thirdInputText: string;
  thirdInputClass: string;

  constructor() {
    this.secondInputText = 'I change automatically! 🤖';
    this.thirdInputText = 'I have a side effect, change me! 🔥';
    this.thirdInputClass = 'is-black';
  }

  ngOnInit() {}

  // Side effect for the third input
  changeThirdInputColor(): void {
    if (this.thirdInputClass === 'is-black') {
      this.thirdInputClass = 'is-red';
    } else {
      this.thirdInputClass = 'is-black';
    }
  }
}
