import { PokemonService } from './../../services/pokemon.service';
import { Component, OnInit } from '@angular/core';

import { IPokemon } from './../../interfaces/pokemon.interface';

@Component({
  selector: 'app-api-example',
  templateUrl: './api-example.component.html',
  styleUrls: ['./api-example.component.scss']
})
export class ApiExampleComponent implements OnInit {
  currentId: number;
  pokemonList: IPokemon[];

  constructor(private pokemonService: PokemonService) {
    // We can use this variable value without subscribing because we are PUSHING
    // to the service array, and that will not change the assigned memory direction
    // of this variable, which is shared with the service array
    this.pokemonList = this.pokemonService.getPokemonList();
    // Dynamically updating variable subscribing to it's Observable
    this.pokemonService.currentId.subscribe(value => this.currentId = value);
   }

  ngOnInit() {
    this.pokemonService.fetchPokemon(this.currentId);
  }

  getNextPokemon(): void {
    const newId = this.currentId + 1;
    this.pokemonService.setIncreaseCurrentId(newId);
  }
}
