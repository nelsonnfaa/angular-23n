import { Component, OnInit } from '@angular/core';

import { IHeroCharacter } from './../../interfaces/hero.interface';

@Component({
  selector: 'app-input-output',
  templateUrl: './input-output.component.html',
  styleUrls: ['./input-output.component.scss']
})
export class InputOutputComponent implements OnInit {
  heroList: IHeroCharacter[];

  constructor() {
    this.heroList = [];
  }

  ngOnInit() {}

  // The form is not being reset for testing purposes, so we can add heroes faster
  pushHero(value: IHeroCharacter): void {
    this.heroList.push(value);
  }
}
